# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.1](https://gitlab.com/0xcccccccc/cicd-alpine-tools-image/compare/v0.3.0...v0.3.1) (2021-07-06)


### Features

* added  python3, py3-pip, yamllint, zsh ([3cb04b3](https://gitlab.com/0xcccccccc/cicd-alpine-tools-image/commit/3cb04b3100c4649c396c97243aa2d5991cc653c9))
