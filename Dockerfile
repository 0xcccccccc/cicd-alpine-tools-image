FROM alpine:3.12.4

RUN apk update && apk add   curl=7.77.0-r0 \
                            bash=5.0.17-r0 \
                            bats=1.2.0-r0 \
                            shellcheck=0.7.1-r1 \
                            gettext=0.20.2-r0 \
                            skopeo=0.2.0-r1 \
                            git=2.26.3-r0 \
                            tree=1.8.0-r0 \
                            jq=1.6-r1 \
                            lftp=4.9.1-r0 \
                            npm=12.22.1-r0 \
                            python3=3.8.10-r0 \
                            py3-pip=20.1.1-r0 \
                            yamllint=1.23.0-r0 \
                            zsh=5.8-r1

RUN npm i -g standard-version@9.3.0
